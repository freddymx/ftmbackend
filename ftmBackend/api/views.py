from datetime import datetime
import json
import mimetypes

from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry, Polygon
from django.contrib.gis.db.models import PointField
from django.db.models.functions import Cast
from django.contrib.gis.measure import D
from django_filters.rest_framework import DjangoFilterBackend
from requests_oauthlib import OAuth1Session
from rest_framework import permissions
from rest_framework.decorators import permission_classes
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework import status

from api import constants
from api.serializers import LocationSerializer, ProfileSerializer
from merchant.models import TwitterCredentials, Merchant


# Create your views here.

class LocationListCreateView(ListCreateAPIView):
    serializer_class = LocationSerializer
    model = LocationSerializer.Meta.model
    queryset = model.objects
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('merchant', )
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, *args, **kwargs):
        return super(LocationListCreateView, self).get(*args, **kwargs)

    def get_queryset(self):
        sw_lat = self.request.query_params.get('sw_lat', None)
        sw_long = self.request.query_params.get('sw_long', None)
        ne_lat = self.request.query_params.get('ne_lat', None)
        ne_long = self.request.query_params.get('ne_long', None)

        bbox = (sw_lat, sw_long, ne_lat, ne_long)

        geom = Polygon().from_bbox(bbox)

        closeby = self.queryset.annotate(geolocation=Cast('location', PointField())).filter(geolocation__contained=geom)
        latest = self.queryset.annotate(geolocation=Cast('location', PointField())).distinct('merchant').order_by('merchant', '-start')

        return closeby.intersection(latest)


class LocationRetrieveUpdateView(RetrieveUpdateAPIView):
    serializer_class = LocationSerializer
    model = LocationSerializer.Meta.model
    queryset = model.objects
    lookup_field = 'id'


# class AuthSocialPlatform(APIView):
#     def get(self, request, platform):
#         consumer = oauth.Consumer(key=settings.TWITTER_API_KEY, secret=settings.TWITTER_API_SECRET)
#         client = oauth.Client(consumer)
#
#         resp, content = client.request(constants.TWITTER_REQUEST_TOKEN_URL, "GET")
#         oauth_token = urllib.parse.parse_qs(content.decode('UTF-8'))['oauth_token'][0]
#         oauth_token_secret = urllib.parse.parse_qs(content.decode('UTF-8'))['oauth_token_secret'][0]
#         return redirect('https://api.twitter.com/oauth/authenticate?oauth_token={}'.format(oauth_token))
#
#
# class TwitterRequestToken(APIView):
#     def get(self, request, *args, **kwargs):
#         consumer = oauth.Consumer(key=settings.TWITTER_API_KEY, secret=settings.TWITTER_API_SECRET)
#         token = oauth.Token(key=request.query_params['oauth_token'], secret=request.query_params['oauth_verifier'])
#         client = oauth.Client(consumer, token)
#
#         body = 'oauth_verifier={}'.format(request.query_params['oauth_verifier'])
#
#         resp, content = client.request(constants.TWITTER_ACCESS_TOKEN_URL, "POST", body=body)
#
#         TwitterCredentials.objects.create(
#             token=urllib.parse.parse_qs(content.decode('UTF-8'))['oauth_token'],
#             secret=urllib.parse.parse_qs(content.decode('UTF-8'))['oauth_token_secret'],
#             merchant=merchant
#         )
#         # return redirect('https://api.twitter.com/oauth/authenticate?oauth_token={}'.format(oauth_token))
#         return Response('{}, {}'.format(request.query_params['oauth_token'], request.query_params['oauth_verifier']))


class SocialPlatformKeys(APIView):
    def post(self, request, platform):
        token = request.data['token']
        secret = request.data['secret']
        handle = request.data['handle']
        user = request.user
        merchant = Merchant.objects.get(user=user)

        if platform == 'twitter':
            TwitterCredentials.objects.get_or_create(merchant=merchant, token=token, secret=secret, handle=handle)

        return Response('success')

    def delete(self, request, platform):
        user = request.user
        merchant = Merchant.objects.get(user=user)
        TwitterCredentials.objects.get(merchant=merchant).delete()


class CheckIn(APIView):
    def post(self, request):
        try:
            creds = TwitterCredentials.objects.get(merchant__user=request.user)
            token = creds.token
            secret = creds.secret

            twitter = OAuth1Session(client_key=settings.TWITTER_API_KEY,
                                    client_secret=settings.TWITTER_API_SECRET,
                                    resource_owner_key=token,
                                    resource_owner_secret=secret)

            twitter_body = {'status': request.data['status']}

            if request.FILES:
                image = request.FILES['image']
                filename = image.name

                file_type = mimetypes.guess_type(filename)
                if file_type is None:
                    return Response(data='Could not determine file type', status=406)
                file_type = file_type[0]
                if file_type not in ['image/gif', 'image/jpeg', 'image/png']:
                    raise TweepError('Invalid file type for image: %s' % file_type, 406)

                files = {"media" : image}
                req_media = twitter.post(constants.TWITTER_MEDIA_UPLOAD_URL, files=files)

                media_id = json.loads(req_media.text)['media_id']

                twitter_body['media_ids'] = [media_id]

            r = twitter.post(constants.TWITTER_STATUS_UPDATE_URL, data=twitter_body)

        except TwitterCredentials.DoesNotExist:
            pass

        lat = request.data.get('latitude')
        lng = request.data.get('longitude')
        status = request.data.get('status')
        address = request.data.get('address')
        start = request.data.get('start')

        if not start:
            start = datetime.now().replace(microsecond=0).isoformat()

        location = LocationSerializer(
            data={'lat': lat, 'lng': lng, 'status': status, 'start': start, 'address': address},
            context={'request': request}
        )
        if location.is_valid():
            location.save()
        else:
            return Response(content, status=status.HTTP_404_NOT_FOUND)


        return Response('success')


class ProfileRetrieveUpdateView(RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    model = ProfileSerializer.Meta.model

    def retrieve(self, *args, **kwargs):
        user = self.request.user
        merchant = self.model.objects.get(user=user)
        profile = self.serializer_class(merchant)
        return Response(data=profile.data, content_type="multipart/mixed", status=status.HTTP_200_OK)

    def update(self, *args, **kwargs):
        user = self.request.user
        data = self.request.data
        merchant = self.model.objects.get(user=user)
        profile = self.serializer_class(merchant, data=data)
        profile.is_valid()
        profile.save()

        return Response(status=status.HTTP_200_OK)


class SocialMediaAuthKeysView(APIView):
    def get(self, request, *args, **kwargs):
        return Response({'twitter_api_key': settings.TWITTER_API_KEY, 'twitter_api_secret': settings.TWITTER_API_SECRET})
