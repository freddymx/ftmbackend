from django.contrib.gis.geos import GEOSGeometry
from rest_framework import serializers

from merchant.models import Location, Merchant, TwitterCredentials


class ProfileSerializer(serializers.ModelSerializer):
    truck_name = serializers.CharField(required=False)
    truck_description = serializers.CharField(required=False)
    truck_website = serializers.CharField(required=False)
    truck_image = serializers.ImageField(required=False)
    twitter_account = serializers.SlugRelatedField(read_only=True, slug_field='handle')

    class Meta:
        model = Merchant
        fields = (
            'id', 'truck_name', 'truck_description',
            'truck_website', 'truck_image', 'twitter_account'
        )

class LocationSerializer(serializers.ModelSerializer):
    merchant = ProfileSerializer(read_only=True)
    start = serializers.DateTimeField(required=False)
    end = serializers.DateTimeField(required=False)
    lat = serializers.CharField(required=True)
    lng = serializers.CharField(required=True)
    status = serializers.CharField(required=False)
    address = serializers.CharField(required=True)

    class Meta:
        model = Location
        fields = ('merchant', 'start', 'end', 'lat', 'lng', 'status', 'address')

    def update(self, instance, validated_data):
        lat = validated_data.pop('lat')
        lng = validated_data.pop('lng')

        geom = GEOSGeometry('POINT({} {})'.format(lat, lng))

        instance.location = geom

        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        instance.save()

        return instance

    def create(self, validated_data):
        user = self.context['request'].user
        merchant = Merchant.objects.get(user=user)
        lat = validated_data.pop('lat')
        lng = validated_data.pop('lng')

        geom = GEOSGeometry('POINT({} {})'.format(lat, lng))

        location = Location.objects.create(merchant=merchant, location=geom, **validated_data)

        return location


class TwitterSerializer(serializers.ModelSerializer):
    class Meta:
        model = TwitterCredentials
        fields = ('token', 'secret', 'handle')
