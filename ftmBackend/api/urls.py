from django.urls import include, path

from api import views as api_views

urlpatterns = [
    path(
        'location/',
        api_views.LocationListCreateView.as_view(),
        name='location_list_view',
    ),
    path(
        'location/<id>/',
        api_views.LocationRetrieveUpdateView.as_view(),
        name='location_get_view',
    ),
    path(
        'social_platform_keys/<platform>/',
        api_views.SocialPlatformKeys.as_view(),
        name='social_platform_keys'
    ),
    path(
        'check_in/',
        api_views.CheckIn.as_view(),
        name='social_status_update'
    ),
    path(
        'profile/',
        api_views.ProfileRetrieveUpdateView.as_view(),
        name='profile_view'
    ),
    path(
        'social_media_auth_keys/',
        api_views.SocialMediaAuthKeysView.as_view(),
        name='social_media_auth_keys'
    ),
]
