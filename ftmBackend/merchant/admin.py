from django.contrib import admin

# Register your models here.
from merchant.models import Location, Merchant

admin.site.register(Location)
admin.site.register(Merchant)
