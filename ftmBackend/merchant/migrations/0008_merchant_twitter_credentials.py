# Generated by Django 2.1.7 on 2019-06-30 11:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('merchant', '0007_twittercredentials_handle'),
    ]

    operations = [
        migrations.AlterField(
            model_name='twittercredentials',
            name='merchant',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='twitter_account', to='merchant.Merchant'),
        ),
    ]
