from django.contrib.auth.models import User
from django.contrib.gis.db import models as gis_models
from django.db import models

# Create your models here.

def nameFile(instance, filename):
    return '/'.join(['images', str(instance.id), 'truck_image.png'])


class Merchant(models.Model):
    user = models.OneToOneField(User, related_name='merchant', on_delete='cascade')
    truck_name = models.CharField(max_length=255, blank=True)
    truck_description = models.TextField(blank=True)
    truck_website = models.CharField(max_length=255, blank=True)
    truck_image = models.ImageField(upload_to=nameFile, max_length=254, blank=True, null=True)

    def __str__(self):
        return f"{self.truck_name}"


class TwitterCredentials(models.Model):
    merchant = models.OneToOneField(Merchant, related_name='twitter_account', on_delete=models.PROTECT)
    token = models.CharField(max_length=255)
    secret = models.CharField(max_length=255)
    handle = models.CharField(max_length=255)

    class Meta:
        unique_together = ('merchant', 'token', 'secret')


class Location(models.Model):
    merchant = models.ForeignKey(Merchant, on_delete=models.PROTECT)
    start = models.DateTimeField()
    end = models.DateTimeField(null=True)
    status = models.CharField(max_length=300, default="")
    location = gis_models.PointField(null=True, geography=True, spatial_index=True)
    address = models.CharField(max_length=255, default="")

    def __str__(self):
        time = self.start.strftime("%m/%d/%Y, %H:%M:%S")
        return f"{self.merchant.truck_name} at {time}"

    @property
    def lat(self):
        return self.location.coords[0]

    @property
    def lng(self):
        return self.location.coords[1]
